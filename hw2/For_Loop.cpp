#include <iostream>
using namespace std;
int main() {
    int a, b;
    cin >> a >> b;///Numbers are taken
    string sayilar[9] = { "one","two","three","four","five","six","seven","eight","nine" };///Array of numbers
    for (int i = a;i <= b;i++)///Loop from number1 to number2
    {
        if (1 <= i && i <= 9)///The condition that i is between 1 and 9
        {
            cout << sayilar[i - 1] << endl;
        }
        else if (i > 9)///The condition that i is bigger than 9
        {
            if (i % 2 == 0)///Checking even numbers
            {
                cout << "even" << endl;
            }
            else///Checking odd numbers
            {
                cout << "odd" << endl;
            }
        }
    }
    return 0;
}