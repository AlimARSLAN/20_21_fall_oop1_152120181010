#include <iostream>
void update(int* pa, int* pb) {
    int temp = *pa, temp1 = *pb;///Keep the pointers in the temp variables
    *pa = temp + temp1;///Sum
    *pb = temp - temp1;///Subtraction
    if (*pb < 0)///The condition of pb is negative
    {
        *pb = -(*pb);///To be positive value
    }
}
int main() {
    int a, b;
    int* pa = &a, * pb = &b;///
    scanf("%d %d", &a, &b);
    update(pa, pb);///pa and pb go to func.
    printf("%d\n%d", a, b);///a and b are written
    return 0;
}