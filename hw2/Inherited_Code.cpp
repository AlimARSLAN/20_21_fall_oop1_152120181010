#include <iostream>
#include <string>
#include <sstream>
#include <exception>
using namespace std;
class BadLengthException :public exception///Exception
{
private:
	int n;
public:
	BadLengthException(int a)///A value equals to n value
	{
		n = a;
	}
	int what()///Return n value
	{
		return n;
	}
};
bool checkUsername(string username) {///Function that check username
	bool isValid = true;
	int n = username.length();
	if (n < 5) {///If length lesser than 5, it goes to exception
		throw BadLengthException(n);
	}
	for (int i = 0; i < n - 1; i++) {///checks that w should not be placed in a row
		if (username[i] == 'w' && username[i + 1] == 'w') {
			isValid = false;
		}
	}
	return isValid;
}
int main() {
	int T; cin >> T;///Number is taken
	while (T--) {
		string username;
		cin >> username;///Username is taken
		try {
			bool isValid = checkUsername(username);///Username goes to checkUsername func.
			if (isValid) {///Printing valir or invalid by looking the isValid value
				cout << "Valid" << '\n';
			}
			else {
				cout << "Invalid" << '\n';
			}
		}
		catch (BadLengthException e) {
			cout << "Too short: " << e.what() << '\n';
		}
	}
	return 0;
}