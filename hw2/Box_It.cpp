#include<iostream>
using namespace std;
class Box///Class named Box
{
private:
    int l, b, h;
public:
    Box()///initialize l,b and h to 0 
    {
        l = 0;
        b = 0;
        h = 0;
    }
    Box(int length, int breadth, int height)///Length,breadth and height values initialize to l,b and h
    {
        this->l = length;
        this->b = breadth;
        this->h = height;
    }
    int getLength()///Returning l
    {
        return l;
    }
    int getBreadth()///Returning b
    {
        return b;
    }
    int getHeight()///Returning h
    {
        return h;
    }
    long long CalculateVolume()///Calculates volume
    {
        return (long long)l * b * h;
    }
    Box(Box& B)///Box B are copied to l,b and h
    {
        l = B.l;
        b = B.b;
        h = B.h;
    }
    bool operator<(Box& B)///The function that calculates greater or lesser 
    {
        if ((l < B.l))
        {
            return true;
        }
        else if ((b < B.b) && (l == B.l))
        {
            return true;
        }
        else if ((h < B.h) && (b == B.b) && (l == B.l))
        {
            return true;
        }
        return false;
    }
    friend ostream& operator<<(ostream& out, Box& B)///Printing the values
    {
        out << B.l << " " << B.b << " " << B.h;
        return out;
    }
};
void check2()
{
    int n;
    cin >> n;///Number is taken
    Box temp;
    for (int i = 0;i < n;i++)
    {
        int type;
        cin >> type;
        if (type == 1)///Writing values of temp object
        {
            cout << temp << endl;
        }
        if (type == 2)
        {
            int l, b, h;
            cin >> l >> b >> h;///Numbers are taken
            Box NewBox(l, b, h);
            temp = NewBox;///Values are stored in another object
            cout << temp << endl;
        }
        if (type == 3)
        {
            int l, b, h;
            cin >> l >> b >> h;///Numbers are taken
            Box NewBox(l, b, h);
            if (NewBox < temp)////The condition of greater or lesser
            {
                cout << "Lesser\n";
            }
            else
            {
                cout << "Greater\n";
            }
        }
        if (type == 4)///Printing the volume
        {
            cout << temp.CalculateVolume() << endl;
        }
        if (type == 5)///Printing the values of NewBox
        {
            Box NewBox(temp);
            cout << NewBox << endl;
        }
    }
}
int main()
{
    check2();
}