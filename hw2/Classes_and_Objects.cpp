#include<iostream>
using namespace std;
class Student
{
private:
    int scores[5];
public:
    void input()
    {
        int num;
        for (int i = 0;i < 5;i++)
        {
            cin >> num;///Numbers are taken
            scores[i] = num;///Those numbers are stored in array
        }
    }
    int calculateTotalScore()
    {
        int sum = 0;
        for (int i = 0;i < 5;i++)
        {
            sum += scores[i];///Scores added up
        }
        return sum;
    }
};
int main() {
    int n; /// number of students
    cin >> n;///number of students is taken
    Student* s = new Student[n]; /// an array of n students
    for (int i = 0; i < n; i++) {
        s[i].input();
    }
    int kristen_score = s[0].calculateTotalScore(); /// calculating kristen's score
    int count = 0;
    for (int i = 1; i < n; i++) {///finding how many students scored higher than kristen
        int total = s[i].calculateTotalScore();
        if (total > kristen_score) {
            count++;
        }
    }
    cout << count; /// print result
    return 0;
}
