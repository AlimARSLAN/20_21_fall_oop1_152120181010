#include <iostream>
#include <string>
using namespace std;

int main() {
    string a, b;
    char temp;
    cin >> a >> b;///Lines are taken
    cout << a.size() << " " << b.size() << endl;///Printing the size of string 
    cout << a + b << endl;///Printing the combination of strings
    temp = a[0];///a string's first value is stored in temp
    a[0] = b[0];///a string's first value is changing
    b[0] = temp;///b string's first value is changing
    cout << a << " " << b;///Printing the new strings
    return 0;
}