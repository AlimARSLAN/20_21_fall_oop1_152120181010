#include <sstream>
#include <vector>
#include <iostream>
using namespace std;
vector<int> parseInts(string str) {
    vector<int> v;///Creating a vector
    stringstream ss(str);///We using stringstream to operate on strings
    int a;
    char c;
    while (ss >> a)
    {
        v.push_back(a);///Number is added to vector;
        ss >> c;///To extract the comma
    }
    return v;
}
int main() {
    string str;
    cin >> str;///Line is taken
    vector<int> integers = parseInts(str);///Goes to func. and its equal to vector that named integers
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";///Writing the elements of vector
    }
    return 0;
}