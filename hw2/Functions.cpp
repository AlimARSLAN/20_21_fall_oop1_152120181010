#include <iostream>
using namespace std;
int Max(int a, int b, int c, int d)///Function named Max
{
    if (a > b && a > c && a > d)///The condition of a is biggest
    {
        return a;
    }
    else if (b > a && b > c && b > d)///The condition of b is biggest
    {
        return b;
    }
    else if (c > a && c > b && c > d)///The condition of c is biggest
    {
        return c;
    }
    else///The condition of d is biggest
    {
        return d;
    }
}
int main() {
    int a, b, c, d;
    cin >> a >> b >> c >> d;///Numbers are taken
    cout << Max(a, b, c, d);///Numbers go to func. and the biggest number is written
    return 0;
}