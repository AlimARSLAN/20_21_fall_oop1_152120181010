#include <iostream>
#include <sstream>
using namespace std;
class Student///Class named Student
{
private:
    int agee;
    int standardd;
    string first_namee;
    string last_namee;
public:
    void set_age(int age)///set the age
    {
        agee = age;
    }
    void set_standard(int standard)///set the standard
    {
        standardd = standard;
    }
    void set_first_name(string first_name)///set the first name
    {
        first_namee = first_name;
    }
    void set_last_name(string last_name)///set the last name
    {
        last_namee = last_name;
    }
    int get_age()///get the age
    {
        return agee;
    }
    string get_last_name()///get the last name
    {
        return last_namee;
    }
    string get_first_name()///get the first name
    {
        return first_namee;
    }
    int get_standard()///get the standard
    {
        return standardd;
    }
    string to_string()///Everything turn into string
    {
        stringstream ss;///We using stringstream to operate on strings
        char c = ',';
        ss << agee << c << first_namee << c << last_namee << c << standardd;
        return ss.str();
    }
};
int main() {
    int age, standard;
    string first_name, last_name;
    cin >> age >> first_name >> last_name >> standard;///Variables are taken
    Student st;
    st.set_age(age);///age go to func
    st.set_standard(standard);///standard go to func
    st.set_first_name(first_name);///first_name go to func
    st.set_last_name(last_name);///last_name go to func
    cout << st.get_age() << "\n";///returning age
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";///returning last name and first name
    cout << st.get_standard() << "\n";///returning standard
    cout << "\n";
    cout << st.to_string();///Printing the values
    return 0;
}